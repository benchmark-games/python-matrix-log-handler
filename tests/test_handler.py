"""Test module for matrix_log_handler"""

import json
import logging

from matrix_log_handler import MatrixLogHandler


def test_logging(mocker):
    """Test if logs actually go to Matrix"""

    handler = MatrixLogHandler('https://example.com', '!test:example.com', token='test_token')
    logger = logging.Logger('test')
    logger.addHandler(handler)

    mocked_response = mocker.MagicMock()
    mocked_response.status_code = 200

    mocked = mocker.patch('matrix_client.api.Session.request', return_value=mocked_response)
    mocker.patch('matrix_client.api.MatrixHttpApi._make_txn_id', return_value='txn')
    mocker.patch('logging.time.time', return_value=1663061094.539326)

    logger.error('Test Message')
    expected_matrix_message = {
        'msgtype': 'm.notice',
        'body': '[ERROR] [2022-09-13 11:24:54,539] [test] - Test Message',
    }

    mocked.assert_called_once_with(
        'PUT',
        'https://example.com/_matrix/client/r0/rooms/%21test%3Aexample.com/send/m.room.message/txn',
        data=json.dumps(expected_matrix_message),
        headers={
            'Authorization': 'Bearer test_token',
            'Content-Type': 'application/json',
            'User-Agent': 'matrix-python-sdk/0.4.0',
        },
        params={},
        verify=True,
    )
